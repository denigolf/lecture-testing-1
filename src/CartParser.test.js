import CartParser from './CartParser';

let parser;
let path;

beforeEach(() => {
  parser = new CartParser();
  path = './samples/cart.csv';
});

describe('CartParser - unit tests', () => {
  it('should not to throw error when a valid path passed', () => {
    expect(() => {
      parser.readFile(path);
    }).not.toThrowError();
  });

  it('should return correct items property length when correct path passed', () => {
    expect(parser.parse(path).items.length).toBe(5);
  });

  it('should return an object with values: name, price, quantity', () => {
    const csvLine = 'Mollis consequat,9.00,2';

    expect(parser.parseLine(csvLine).name).toBe('Mollis consequat');
    expect(parser.parseLine(csvLine).price).toBe(9);
    expect(parser.parseLine(csvLine).quantity).toBe(2);
  });

  it('should return NaN when invalid value passed to the cell with a type Number', () => {
    const csvLine = `Mollis consequat,'test',2`;

    expect(parser.parseLine(csvLine).price).toBeNaN();
  });

  it('should return the total price of the items in the cart', () => {
    const cartItems = [
      {
        id: '3e6def17-5e87-4f27-b6b8-ae78948523a9',
        name: 'Mollis consequat',
        price: 9,
        quantity: 2,
      },
      {
        id: '90cd22aa-8bcf-4510-a18d-ec14656d1f6a',
        name: 'Tvoluptatem',
        price: 10.32,
        quantity: 1,
      },
      {
        id: '33c14844-8cae-4acd-91ed-6209a6c0bc31',
        name: 'Scelerisque lacinia',
        price: 18.9,
        quantity: 1,
      },
    ];

    expect(parser.calcTotal(cartItems)).toBe(47.22);
  });

  it('should return an array with length grater than 0 when header or headers is wrong', () => {
    const headers = 'Product name, Price, Quan';

    expect(parser.validate(headers).length).not.toBe(0);
  });

  it('should return an array with length equal to 1 when one cell number is not positive', () => {
    const contents = `Product name, Price, Quantity
		Mollis consequat,9.00,-2`;

    expect(parser.validate(contents).length).toBe(1);
  });

  it('should return an array with length equal to 1 when one cell type is not a Number', () => {
    const contentsWithNull = `Product name, Price, Quantity
		Mollis consequat,9.00,${null}`;

    const contentsWithUndefined = `Product name, Price, Quantity
		Mollis consequat,9.00,${undefined}`;

    const contentsWithNaN = `Product name, Price, Quantity
		Mollis consequat,9.00,${NaN}`;

    const contentsWithObject = `Product name, Price, Quantity
		Mollis consequat,9.00,${{}}`;

    const contentsWithString = `Product name, Price, Quantity
		Mollis consequat,9.00,${'test'}`;

    const contentsWithBool = `Product name, Price, Quantity
		Mollis consequat,9.00,${false}`;

    expect(parser.validate(contentsWithNull).length).toBe(1);
    expect(parser.validate(contentsWithUndefined).length).toBe(1);
    expect(parser.validate(contentsWithNaN).length).toBe(1);
    expect(parser.validate(contentsWithObject).length).toBe(1);
    expect(parser.validate(contentsWithString).length).toBe(1);
    expect(parser.validate(contentsWithBool).length).toBe(1);
  });

  it('should return an array with length equal to 1 when row has less than 3 cells', () => {
    const contents = `Product name, Price, Quantity
		Mollis consequat,`;

    expect(parser.validate(contents).length).toBe(1);
  });

  it('should return an array with length equal to 1 when cell is an empty string', () => {
    const contents = `Product name, Price, Quantity
		,,`;

    expect(parser.validate(contents).length).toBe(1);
  });

  it('should not to throw error when valid file path passed and no validation errors', () => {
    expect(() => {
      parser.parse(path);
    }).not.toThrowError();
  });
});

describe('CartParser - integration test', () => {
  it('should correctly parse the file', () => {
    expect(() => {
      parser.readFile(path);
    }).not.toThrowError();

    const contents = parser.readFile(path);

    expect(parser.validate(contents).length).toBe(0);

    expect(() => {
      parser.parse(path);
    }).not.toThrowError();

    const totalSum = 348.32;
    const items = parser.parse(path).items;

    expect(parser.calcTotal(items)).toBeCloseTo(totalSum);

    expect(parser.parse(path).total).toBe(totalSum);
  });
});
